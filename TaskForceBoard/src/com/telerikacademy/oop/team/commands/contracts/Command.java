package com.telerikacademy.oop.team.commands.contracts;

import java.util.List;

public interface Command {

    String execute(List<String> parameters);

}