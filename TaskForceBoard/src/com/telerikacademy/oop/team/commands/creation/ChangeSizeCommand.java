package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.models.tasks.contracts.Story;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.utils.ParsingHelpers;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.INVALID_COMMAND;
import static com.telerikacademy.oop.team.models.tasks.ModelConstants.INVALID_INPUT_MESSAGE;


public class ChangeSizeCommand extends BaseCommand {


    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String SIZE_CHANGED_MESSAGE = "Size of %s with ID %d changed to %s.";
    private TaskRepositoryHelper taskRepositoryHelper;

    public ChangeSizeCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int taskId = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_INPUT_MESSAGE);
        String command = parameters.get(1);

        return changeSize(taskId, command);
    }

    private String changeSize(int taskId, String command) {
        Task task = taskRepositoryHelper.findElementById(getTaskRepository().getTasks(), taskId);
        TaskType type = task.getType();

        ifInvalidType(type);

        Story story = taskRepositoryHelper.findElementById(getTaskRepository().getStory(), taskId);
        switch (command) {
            case "advance":
                story.advanceSize();
                break;
            case "revert":
                story.revertSize();
                break;
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, type));
        }
        return String.format(SIZE_CHANGED_MESSAGE, type, taskId, story.getSize().toString());
    }

    private void ifInvalidType(TaskType type) {
        boolean isValid = type.equals(TaskType.STORY);
        if (!isValid) {
            throw new IllegalArgumentException(String.format(INVALID_COMMAND, "Story"));
        }
    }
}
