package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Bug;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;


public class CreateBugCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String TASK_CREATED_MESSAGE = "Bug with ID %d was created.";

    public CreateBugCommand(TaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        return createBug(parameters);
    }

    private String createBug(List<String> parameters) {

        Bug bug = getTaskRepository().createBug(parameters.get(0), parameters.get(1));
        bug.addToTaskHistory(String.format(TASK_CREATED_MESSAGE, bug.getId()));
        return String.format(TASK_CREATED_MESSAGE, bug.getId());
    }
}
