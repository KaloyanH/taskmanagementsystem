package com.telerikacademy.oop.team.commands.creation;


import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.models.tasks.contracts.Bug;
import com.telerikacademy.oop.team.models.tasks.contracts.Member;
import com.telerikacademy.oop.team.models.tasks.contracts.Story;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.DEFAULT_ASSIGNEE;
import static com.telerikacademy.oop.team.models.tasks.ModelConstants.MEMBER_DOES_NOT_EXIST_IN_TEAM;

public class UnAssignTaskCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String UNASSIGNED_TASK_FROM_MEMBER = "%s with ID %d was unassigned from %s";
    private TaskRepositoryHelper taskRepositoryHelper;


    public UnAssignTaskCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int taskID = Integer.parseInt(parameters.get(0));
        String memberName = parameters.get(1);

        return unAssignTask(taskID, memberName);
    }

    private String unAssignTask(int taskID, String memberName) {


        Task task = taskRepositoryHelper.findElementById(getTaskRepository().getTasks(), taskID);

        Member member = taskRepositoryHelper.findMemberByName(memberName);


        TaskType type = task.getType();
        boolean isInTeam = taskRepositoryHelper.findMemberInTeam(memberName);
        if (isInTeam) {
            switch (type) {
                case BUG:
                    Bug bug = taskRepositoryHelper.findElementById(taskRepositoryHelper.getTaskRepo().getBug(), taskID);
                    bug.setAssignee(DEFAULT_ASSIGNEE);
                    member.getMemberTaskList().remove(bug);
                    break;
                case STORY:
                    Story story = taskRepositoryHelper.findElementById(taskRepositoryHelper.getTaskRepo().getStory(), taskID);
                    story.setAssignee(DEFAULT_ASSIGNEE);
                    member.getMemberTaskList().remove(story);
                    break;
            }
        } else {
            throw new IllegalArgumentException(String.format(MEMBER_DOES_NOT_EXIST_IN_TEAM, memberName));
        }
        String nameOfTeam = member.getTeam();
        task.addToTaskHistory(String.format(UNASSIGNED_TASK_FROM_MEMBER, type, taskID, memberName));
        member.addToMemberHistory(String.format(UNASSIGNED_TASK_FROM_MEMBER, type, taskID, memberName));
        taskRepositoryHelper.findTeamByName(nameOfTeam).addToTeamHistory(String.format(UNASSIGNED_TASK_FROM_MEMBER, type, taskID, memberName));

        return String.format(UNASSIGNED_TASK_FROM_MEMBER, type, taskID, memberName);

    }
}
