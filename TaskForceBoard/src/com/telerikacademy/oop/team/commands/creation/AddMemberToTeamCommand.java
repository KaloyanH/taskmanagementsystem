package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Member;
import com.telerikacademy.oop.team.models.tasks.contracts.Team;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

public class AddMemberToTeamCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String MEMBER_ADDED_TO_TEAM = "Member '%s' was added to team '%s'!";

    private TaskRepositoryHelper taskRepositoryHelper;

    public AddMemberToTeamCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String memberToAdd = parameters.get(0);
        String teamToAddMemberTo = parameters.get(1);
        return addMemberToTeam(memberToAdd, teamToAddMemberTo);
    }

    private String addMemberToTeam(String memberToAdd, String teamToAddMemberTo) {

        Member member = taskRepositoryHelper.findMemberByName(memberToAdd);


        Team team = taskRepositoryHelper.findTeamByName(teamToAddMemberTo);
        team.addMember(member);
        member.setTeam(teamToAddMemberTo);
        member.addToMemberHistory(String.format(MEMBER_ADDED_TO_TEAM, memberToAdd, teamToAddMemberTo));
        taskRepositoryHelper.findTeamByName(teamToAddMemberTo).addToTeamHistory(String.format(MEMBER_ADDED_TO_TEAM, memberToAdd, teamToAddMemberTo));
        return String.format(MEMBER_ADDED_TO_TEAM, memberToAdd, teamToAddMemberTo);
    }
}
