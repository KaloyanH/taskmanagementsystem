package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Comment;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.utils.ParsingHelpers;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

public class ShowCommentsCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String INVALID_INPUT_MESSAGE = "Invalid input. Expected a number.";
    public final static String TASK_DOES_NOT_EXIST = "The task does not exist!";
    private TaskRepositoryHelper taskRepositoryHelper;

    public ShowCommentsCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        int taskID = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_INPUT_MESSAGE) - 1;
        return showComments(taskID);
    }

    private String showComments(int taskID) {
        ValidationHelper.validateIntRange(taskID, 0, getTaskRepository().getTasks().size(), TASK_DOES_NOT_EXIST);
        Task task = getTaskRepository().getTasks().get(taskID);
        List<Comment> taskComments = task.getComments();
        StringBuilder sb = new StringBuilder();
        for (Comment taskComment : taskComments) {
            sb.append(taskComment.toString());
        }
        return sb.toString();
    }
}
