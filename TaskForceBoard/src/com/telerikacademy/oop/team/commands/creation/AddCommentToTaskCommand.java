package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Comment;
import com.telerikacademy.oop.team.models.tasks.contracts.Member;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.utils.ParsingHelpers;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.INVALID_INPUT_MESSAGE;

public class AddCommentToTaskCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    public final static String COMMENT_ADDED_SUCCESSFULLY = "%s added comment successfully!";


    private TaskRepositoryHelper taskRepositoryHelper;


    public AddCommentToTaskCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {

        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String content = parameters.get(0);
        String author = parameters.get(1);

        int taskID = ParsingHelpers.tryParseInteger(parameters.get(2), INVALID_INPUT_MESSAGE);

        return addComment(content, taskID, author);

    }

    private String addComment(String content, int taskID, String author) {

        Member member = taskRepositoryHelper.findMemberByName(author);

        Task task = taskRepositoryHelper.findElementById(member.getMemberTaskList(), taskID);

        Comment comment = getTaskRepository().createComment(content, member.getMemberName());
        taskRepositoryHelper.findMemberByName(member.getMemberName()).addComment(comment, task);

        return String.format(COMMENT_ADDED_SUCCESSFULLY, member.getMemberName());
    }
}
