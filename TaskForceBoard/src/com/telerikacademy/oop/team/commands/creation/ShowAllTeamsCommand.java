package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Team;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

public class ShowAllTeamsCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;
    private TaskRepositoryHelper taskRepositoryHelper;

    public ShowAllTeamsCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        return showAllTeams();
    }

    private String showAllTeams() {
        List<Team> teams = getTaskRepository().getTeams();
        int counter = 1;
        StringBuilder sb = new StringBuilder();
        sb.append(TEAMS_HEADER);
        sb.append(System.lineSeparator());
        if (teams.isEmpty()) {
            sb.append(NO_TEAMS);
        }
        for (Team team : teams) {
            sb.append(String.format("%d. ", counter++));
            sb.append(team.getTeamName()).append(System.lineSeparator());
        }
        sb.append(System.lineSeparator()).append(HEADER_FOOTER);
        return sb.toString();
    }
}
