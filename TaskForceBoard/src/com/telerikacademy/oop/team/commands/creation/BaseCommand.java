package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.commands.contracts.Command;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;

import java.util.List;

public abstract class BaseCommand implements Command {

    private final TaskRepository taskRepository;

    protected BaseCommand(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    protected TaskRepository getTaskRepository() {
        return taskRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        return executeCommand(parameters);
    }

    protected abstract String executeCommand(List<String> parameters);
}
