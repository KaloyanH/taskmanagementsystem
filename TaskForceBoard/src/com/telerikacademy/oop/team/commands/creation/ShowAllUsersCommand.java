package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Member;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

public class ShowAllUsersCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;
    private TaskRepositoryHelper taskRepositoryHelper;

    public ShowAllUsersCommand(TaskRepository taskRepository) {
        super(taskRepository);

        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        return showAllUsers();
    }

    private String showAllUsers() {
        List<Member> allUsersList = getTaskRepository().getMembers();
        int counter = 1;
        StringBuilder sb = new StringBuilder();
        sb.append(USERS_HEADER);
        sb.append(System.lineSeparator());
        if (allUsersList.isEmpty()) {
            sb.append(NO_MEMBERS);
        }
        for (Member member : allUsersList) {
            sb.append(String.format("%d. ", counter++));
            sb.append(member.getMemberName()).append(System.lineSeparator());

        }
        sb.append(HEADER_FOOTER);
        return sb.toString();
    }
}
