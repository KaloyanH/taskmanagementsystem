package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.models.tasks.contracts.FeedBack;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.utils.ParsingHelpers;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.INVALID_INPUT_MESSAGE;

public class ChangeRatingCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String INVALID_TASK_TYPE = "Invalid task type. You can only change rating tasks of type FeedBack.";
    public static final String RATING_CHANGED_MESSAGE = "Rating of Feedback with ID %d changed to %d.";

    private TaskRepositoryHelper taskRepositoryHelper;

    public ChangeRatingCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int feedbackID = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_INPUT_MESSAGE);
        int rating = ParsingHelpers.tryParseInteger(parameters.get(1), INVALID_INPUT_MESSAGE);
        return changeRating(feedbackID, rating);
    }

    private String changeRating(int id, int rating) {
        Task task = taskRepositoryHelper.findElementById(getTaskRepository().getTasks(), id);

        ifInvalidTaskType(task);

        FeedBack feedBack = taskRepositoryHelper.findElementById(getTaskRepository().getFeedback(), id);
        feedBack.changeRating(rating);
        feedBack.addToTaskHistory(String.format(RATING_CHANGED_MESSAGE, id, rating));

        return String.format(RATING_CHANGED_MESSAGE, id, rating);
    }

    private void ifInvalidTaskType(Task task) {
        boolean ifInvalidTaskType = task.getType().equals(TaskType.FEEDBACK);
        if (!ifInvalidTaskType) {
            throw new IllegalArgumentException(INVALID_TASK_TYPE);
        }
    }
}
