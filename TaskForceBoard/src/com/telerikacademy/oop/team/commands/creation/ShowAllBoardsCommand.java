package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Board;
import com.telerikacademy.oop.team.models.tasks.contracts.Team;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

public class ShowAllBoardsCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String NO_BOARDS_CREATED_FOR_TEAM = "There are no boards created for team '%s'.";
    private TaskRepositoryHelper taskRepositoryHelper;

    public ShowAllBoardsCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String teamName = parameters.get(0);

        return showAllTeamBoards(teamName);
    }

    private String showAllTeamBoards(String teamName) {
        Team team = taskRepositoryHelper.findTeamByName(teamName);
        List<Board> boards = team.getBoardList();
        int counter = 1;
        if (boards.isEmpty()) {
            throw new IllegalArgumentException(
                    String.format(NO_BOARDS_CREATED_FOR_TEAM, team.getTeamName()));
        }
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(TEAM_BOARDS_HEADER, teamName));
        sb.append(System.lineSeparator());
        for (Board board : boards) {
            sb.append(String.format("%d. ", counter++));
            sb.append(board.getName()).append(System.lineSeparator());
        }
        sb.append(HEADER_FOOTER);

        return sb.toString();
    }
}
