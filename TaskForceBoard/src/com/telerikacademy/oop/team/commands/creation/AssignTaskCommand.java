package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.*;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

public class AssignTaskCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String ASSIGNED_TASK_TO_MEMBER = "%s with id %d was assigned to %s";
    private static final String TASK_ALREADY_ASSIGNED = "%s is already assigned";

    private TaskRepositoryHelper taskRepositoryHelper;

    public AssignTaskCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);

    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int idOfTask = Integer.parseInt(parameters.get(0));
        String assignee = parameters.get(1);

        return assignTask(assignee, idOfTask);
    }

    private String assignTask(String assignee, int idOfTask) {


        Task task = taskRepositoryHelper.findElementById(getTaskRepository().getTasks(), idOfTask);

        Member member = taskRepositoryHelper.findMemberByName(assignee);
        TaskType type = task.getType();

        throwIfMemberIsNotInTeam(assignee);

        switch (type) {
            case BUG:

                Bug bug = taskRepositoryHelper.findElementById(getTaskRepository().getBug(), idOfTask);
                if (!bug.getAssignee().equals(DEFAULT_ASSIGNEE)) {
                    throw new IllegalArgumentException(String.format(TASK_ALREADY_ASSIGNED, "Bug"));
                }
                bug.setAssignee(assignee);
                member.getMemberTaskList().add(bug);

                break;
            case STORY:

                Story story = taskRepositoryHelper.findElementById(getTaskRepository().getStory(), idOfTask);
                if (!story.getAssignee().equals(DEFAULT_ASSIGNEE)) {
                    throw new IllegalArgumentException(String.format(TASK_ALREADY_ASSIGNED, "Story"));
                }
                story.setAssignee(assignee);
                member.getMemberTaskList().add(story);

                break;
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, type));
        }

        addHistory(assignee, idOfTask, task, member, type);

        return String.format(ASSIGNED_TASK_TO_MEMBER, type, idOfTask, assignee);

    }

    private void throwIfMemberIsNotInTeam(String assignee) {

        boolean isInTeam = taskRepositoryHelper.findMemberInTeam(assignee);
        if (!isInTeam) {

            throw new IllegalArgumentException(String.format(MEMBER_DOES_NOT_EXIST_IN_TEAM, assignee));

        }

    }


    private void addHistory(String assignee, int idOfTask, Task task, Member member, TaskType type) {

        String nameOfTeam = member.getTeam();
        task.addToTaskHistory(String.format(ASSIGNED_TASK_TO_MEMBER, type, idOfTask, assignee));
        member.addToMemberHistory(String.format(ASSIGNED_TASK_TO_MEMBER, type, idOfTask, assignee));
        taskRepositoryHelper.findTeamByName(nameOfTeam).addToTeamHistory(String.format(ASSIGNED_TASK_TO_MEMBER, type, idOfTask, assignee));

    }

}
