package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Bug;
import com.telerikacademy.oop.team.models.tasks.contracts.Story;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.utils.ParsingHelpers;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.INVALID_COMMAND;
import static com.telerikacademy.oop.team.models.tasks.ModelConstants.INVALID_INPUT_MESSAGE;

public class ChangePriorityCommand extends BaseCommand {


    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String CHANGED_PRIORITY = "Task with type '%s' changed its priority to '%s'!";
    private static final String NO_SUCH_PRIORITY = "There's no such priority";

    private TaskRepositoryHelper taskRepositoryHelper;

    public ChangePriorityCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {

        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_INPUT_MESSAGE );
        String command = parameters.get(1);


        return changePriority(taskId, command);
    }

    private String changePriority(int idOfTask, String command) {

        Task task = taskRepositoryHelper.findElementById(getTaskRepository().getTasks(), idOfTask);
        TaskType type = task.getType();

        switch (type) {
            case BUG:
                Bug bug = taskRepositoryHelper.findElementById(getTaskRepository().getBug(), idOfTask);
                switch (command) {
                    case "advance":
                        bug.advancePriority();
                        break;
                    case "revert":
                        bug.revertPriority();
                        break;
                    default:
                        throw new IllegalArgumentException(String.format(INVALID_COMMAND, type));
                }
                return String.format(CHANGED_PRIORITY, type, bug.getPriority().toString());
            case STORY:
                Story story = taskRepositoryHelper.findElementById(getTaskRepository().getStory(), idOfTask);
                switch (command) {
                    case "advance":
                        story.advancePriority();
                        break;
                    case "revert":
                        story.revertPriority();
                        break;
                    default:
                        throw new IllegalArgumentException(String.format(INVALID_COMMAND, type));

                }
                return String.format(CHANGED_PRIORITY, type, story.getPriority().toString());
        }
        return NO_SUCH_PRIORITY;
    }
}

