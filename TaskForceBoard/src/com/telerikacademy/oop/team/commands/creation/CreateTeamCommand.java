package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Team;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

public class CreateTeamCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String TEAM_ALREADY_EXISTS_ERROR = "Team \"%s\" already exists. Choose a different team name!";
    public static final String CREATED_TEAM = "Created team \"%s\"";
    private TaskRepositoryHelper taskRepositoryHelper;

    public CreateTeamCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String teamName = parameters.get(0);

        throwIfTeamNameAlreadyExists(teamName);

        return createTeam(teamName);
    }

    private String createTeam(String teamName) {
        Team createdTeam = getTaskRepository().createTeam(teamName);
        taskRepositoryHelper.findTeamByName(teamName).addToTeamHistory(String.format(CREATED_TEAM, createdTeam.getTeamName()));
        return String.format(CREATED_TEAM, createdTeam.getTeamName());
    }

    private void throwIfTeamNameAlreadyExists(String teamName) {

        if (getTaskRepository().getTeams().stream().anyMatch(team -> team.getTeamName().equalsIgnoreCase(teamName))) {
            throw new IllegalArgumentException(String.format(TEAM_ALREADY_EXISTS_ERROR, teamName));
        }
    }
}