package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Bug;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.utils.ParsingHelpers;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.INVALID_COMMAND;
import static com.telerikacademy.oop.team.models.tasks.ModelConstants.INVALID_INPUT_MESSAGE;


public class ChangeSeverityCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String SEVERITY_CHANGED_MESSAGE = "Severity of %s with ID %d changed to %s.";
    private TaskRepositoryHelper taskRepositoryHelper;

    public ChangeSeverityCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_INPUT_MESSAGE);
        String command = parameters.get(1);

        return changeSeverity(taskId, command);


    }

    private String changeSeverity(int taskId, String command) {
        Task task = taskRepositoryHelper.findElementById(getTaskRepository().getTasks(), taskId);
        TaskType type = task.getType();

        ifInvalidType(type);

        Bug bug = taskRepositoryHelper.findElementById(getTaskRepository().getBug(), taskId);
        switch (command) {
            case "advance":
                bug.advanceSeverity();
                break;
            case "revert":
                bug.revertSeverity();
                break;
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, type));
        }
        return String.format(SEVERITY_CHANGED_MESSAGE, type, taskId, bug.getSeverity().toString());
    }

    private void ifInvalidType(TaskType type) {
        boolean isValidCommand = type.equals(TaskType.BUG);
        if (!isValidCommand) {
            throw new IllegalArgumentException(String.format(INVALID_COMMAND, "Bug"));
        }
    }
}
