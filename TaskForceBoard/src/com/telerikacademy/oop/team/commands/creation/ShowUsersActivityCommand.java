package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

public class ShowUsersActivityCommand extends BaseCommand {
    private TaskRepositoryHelper taskRepositoryHelper;


    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public ShowUsersActivityCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }


    @Override
    protected String executeCommand(List<String> parameters) {

        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        return taskRepositoryHelper.getMemberHistory(parameters.get(0));
    }
}
