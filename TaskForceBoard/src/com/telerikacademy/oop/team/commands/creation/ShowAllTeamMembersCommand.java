package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Member;
import com.telerikacademy.oop.team.models.tasks.contracts.Team;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

public class ShowAllTeamMembersCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String NO_MEMBERS_IN_TEAM = "There are no members in team '%s'.";
    private TaskRepositoryHelper taskRepositoryHelper;

    public ShowAllTeamMembersCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String teamName = parameters.get(0);

        return showAllTeamMembers(teamName);
    }

    public String showAllTeamMembers(String teamName) {
        Team team = taskRepositoryHelper.findTeamByName(teamName);
        List<Member> teamMemberList = team.getMemberList();
        int counter = 1;
        if (teamMemberList.isEmpty()) {
            throw new IllegalArgumentException(
                    String.format(NO_MEMBERS_IN_TEAM, team.getTeamName()));
        }
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(TEAM_MEMBERS_HEADER, teamName));
        sb.append(System.lineSeparator());
        for (Member member : teamMemberList) {
            sb.append(String.format("%d. ", counter++));
            sb.append(member.getMemberName()).append(System.lineSeparator());
        }
        sb.append(HEADER_FOOTER);

        return sb.toString();
    }
}
