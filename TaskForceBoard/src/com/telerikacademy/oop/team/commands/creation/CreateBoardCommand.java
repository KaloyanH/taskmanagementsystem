package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Board;
import com.telerikacademy.oop.team.models.tasks.contracts.Team;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

public class CreateBoardCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String BOARD_ADDED_TO_TEAM = "Board '%s' was added to team '%s'!";
    private static final String BOARD_ALREADY_EXISTS_ERROR = "Board \"%s\" already exists. Choose a different board name!";

    private TaskRepositoryHelper taskRepositoryHelper;

    public CreateBoardCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String boardToAdd = parameters.get(0);
        String teamToAddBoardTo = parameters.get(1);

        return addBoardToTeam(boardToAdd, teamToAddBoardTo);
    }

    private String addBoardToTeam(String boardToAdd, String teamToAddBoardTo){
        throwIfBoardNameAlreadyExists(boardToAdd);
        Team team = taskRepositoryHelper.findTeamByName(teamToAddBoardTo);
        Board createdBoard = getTaskRepository().createTeamBoard(boardToAdd);
        team.addBoard(createdBoard);
        taskRepositoryHelper.findTeamByName(teamToAddBoardTo).addToTeamHistory(String.format(BOARD_ADDED_TO_TEAM, boardToAdd, teamToAddBoardTo));
        createdBoard.addToBoardHistory(String.format(BOARD_ADDED_TO_TEAM, boardToAdd, teamToAddBoardTo));
        return String.format(BOARD_ADDED_TO_TEAM, boardToAdd, teamToAddBoardTo);
    }

    private void throwIfBoardNameAlreadyExists(String boardName){
        if (getTaskRepository().getBoards().stream().anyMatch(board -> board.getName().equalsIgnoreCase(boardName))) {
            throw new IllegalArgumentException(String.format(BOARD_ALREADY_EXISTS_ERROR, boardName));
        }
    }
}