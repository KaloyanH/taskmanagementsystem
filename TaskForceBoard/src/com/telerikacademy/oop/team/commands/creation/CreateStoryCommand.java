package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Story;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

public class CreateStoryCommand extends BaseCommand {
    private TaskRepositoryHelper taskRepositoryHelper;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String TASK_CREATED_MESSAGE = "Story with ID %d was created.";

    public CreateStoryCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        Story story = getTaskRepository().createStory(parameters.get(0), parameters.get(1));

        story.addToTaskHistory(String.format(TASK_CREATED_MESSAGE, story.getId()));
        return String.format(TASK_CREATED_MESSAGE, story.getId());
    }
}
