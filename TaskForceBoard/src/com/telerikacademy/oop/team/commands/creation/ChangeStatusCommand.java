package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.utils.ParsingHelpers;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.INVALID_COMMAND;
import static com.telerikacademy.oop.team.models.tasks.ModelConstants.INVALID_INPUT_MESSAGE;

public class ChangeStatusCommand extends BaseCommand {


    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String STATUS_CHANGED_MESSAGE = "Status of %s with ID %d changed to %s.";
    private TaskRepositoryHelper taskRepositoryHelper;

    public ChangeStatusCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int taskId = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_INPUT_MESSAGE);
        String command = parameters.get(1);

        return changeStatus(taskId, command);
    }

    private String changeStatus(int taskId, String command) {
        Task task = taskRepositoryHelper.findElementById(getTaskRepository().getTasks(), taskId);
        TaskType type = task.getType();

        switch (command) {
            case "advance":
                task.advanceStatus();
                break;
            case "revert":
                task.revertStatus();
                break;
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, type));
        }
        return String.format(STATUS_CHANGED_MESSAGE, type, taskId, task.getStatus().toString());
    }
}
