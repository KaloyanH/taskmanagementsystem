package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Bug;
import com.telerikacademy.oop.team.utils.ParsingHelpers;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.INVALID_INPUT_MESSAGE;


public class AddStepsToBugCommand extends BaseCommand {
    private TaskRepositoryHelper taskRepositoryHelper;
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String STEPS_ADDED_TO_BUG = "Step '%s' was added to bug with ID %d!";


    public AddStepsToBugCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int taskId = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_INPUT_MESSAGE);
        String step = parameters.get(1);


        return addSteps(taskId, step);
    }

    private String addSteps(int taskId, String step) {
        Bug bug = taskRepositoryHelper.findElementById(getTaskRepository().getBug(), taskId);

        bug.addToStepsList(step);

        bug.addToTaskHistory(String.format(STEPS_ADDED_TO_BUG, step, taskId));

        return String.format(STEPS_ADDED_TO_BUG, step, taskId);
    }

}
