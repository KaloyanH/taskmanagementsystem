package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Member;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

public class CreateMemberCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String MEMBER_ALREADY_EXISTS_ERROR = "Member \"%s\" already exists. Choose a different member name!";
    public static final String CREATED_MEMBER = "Created member \"%s\"";
    private TaskRepositoryHelper taskRepositoryHelper;


    public CreateMemberCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String memberName = parameters.get(0);

        throwIfMemberNameAlreadyExists(memberName);

        return createMember(memberName);
    }

    private String createMember(String memberName) {
        Member createdMember = getTaskRepository().createMember(memberName);

        addToHistory(createdMember);

        return String.format(CREATED_MEMBER, createdMember.getMemberName());
    }

    private void addToHistory(Member createdMember) {
        createdMember.addToMemberHistory(String.format(CREATED_MEMBER, createdMember.getMemberName()));
        taskRepositoryHelper.getTeamHistory(String.format(CREATED_MEMBER, createdMember.getMemberName()));
        taskRepositoryHelper.getBoardHistory(String.format(CREATED_MEMBER, createdMember.getMemberName()));
    }

    private void throwIfMemberNameAlreadyExists(String memberName) {
        if (getTaskRepository().getMembers().stream().anyMatch(member -> member.getMemberName().equalsIgnoreCase(memberName))) {
            throw new IllegalArgumentException(String.format(MEMBER_ALREADY_EXISTS_ERROR, memberName));
        }
    }
}