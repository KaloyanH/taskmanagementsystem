package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Board;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

public class AddTaskToBoardCommand extends BaseCommand {

    private TaskRepositoryHelper taskRepositoryHelper;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String TASK_ADDED_TO_BOARD = "Task '%s' of type %s was added to board '%s'!";
    private static final String TASK_ALREADY_ADDED_TO_BOARD = "Task with id: '%d' is already added to board";

    public AddTaskToBoardCommand(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {

        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int idOfTask = Integer.parseInt(parameters.get(0));
        String boardToAddBoardTo = parameters.get(1);

//        return throwIfIsNotAddedToBoard(idOfTask, boardToAddBoardTo);
        return addBoardToTeam(idOfTask, boardToAddBoardTo);
    }


    private String addBoardToTeam(int taskToAdd, String boardToAddBoardTo) {

        Task task = taskRepositoryHelper.findElementById(getTaskRepository().getTasks(), taskToAdd);
        TaskType type = task.getType();

        Board board = taskRepositoryHelper.findBoardByName(boardToAddBoardTo);
        board.getTasksList().add(task);

        addToBoardHistory(taskToAdd, type, boardToAddBoardTo, board);

        return String.format(TASK_ADDED_TO_BOARD, taskToAdd, type, boardToAddBoardTo);

    }

    private void addToBoardHistory(int taskToAdd, TaskType type, String boardToAddBoardTo, Board board) {

        board.addToBoardHistory(String.format(TASK_ADDED_TO_BOARD, taskToAdd, type, boardToAddBoardTo));

    }


}
