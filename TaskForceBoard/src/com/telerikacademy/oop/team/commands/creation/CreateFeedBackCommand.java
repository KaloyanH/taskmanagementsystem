package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.FeedBack;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.List;

public class CreateFeedBackCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    public static final String TASK_CREATED_MESSAGE = "Feedback with ID %d was created.";

    public CreateFeedBackCommand(TaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        return createFeedback(parameters);
    }

    private String createFeedback(List<String> parameters) {
        FeedBack feedBack = getTaskRepository().createFeedBack(parameters.get(0), parameters.get(1), Integer.parseInt(parameters.get(2)));
        feedBack.addToTaskHistory(String.format(TASK_CREATED_MESSAGE, feedBack.getId()));
        return String.format(TASK_CREATED_MESSAGE, feedBack.getId());
    }
}
