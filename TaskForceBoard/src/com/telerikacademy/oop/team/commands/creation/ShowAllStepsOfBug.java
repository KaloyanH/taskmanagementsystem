package com.telerikacademy.oop.team.commands.creation;

import com.telerikacademy.oop.team.core.TaskRepositoryHelper;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.tasks.contracts.Bug;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.HEADER_FOOTER;

public class ShowAllStepsOfBug extends BaseCommand {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String NO_STEPS_IN_BUG = "There are no steps in bug with id %d.";
    public static final String STEPS_HEADER = "---STEPS OF BUG---";
    private TaskRepositoryHelper taskRepositoryHelper;

    public ShowAllStepsOfBug(TaskRepository taskRepository) {
        super(taskRepository);
        taskRepositoryHelper = new TaskRepositoryHelper(taskRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        int taskId = Integer.parseInt(parameters.get(0));

        return showAllStepsOfBug(taskId);
    }

    public String showAllStepsOfBug(int taskId) {
        List<Bug> bug = getTaskRepository().getBug();
        List<String> stepsList = new ArrayList<>();
        for (Bug bug1 : bug) {
            stepsList = bug1.getStepsList();
            break;
        }
        int counter = 1;
        ifListIsEmty(taskId, stepsList);
        StringBuilder sb = new StringBuilder();
        sb.append(STEPS_HEADER);
        sb.append(System.lineSeparator());
        for (String steps : stepsList) {
            sb.append(String.format("%d. ", counter++));
            sb.append(steps).append(System.lineSeparator());
        }
        sb.append(HEADER_FOOTER);

        return sb.toString();
    }

    private void ifListIsEmty(int taskId, List<String> stepsList) {
        boolean empty = stepsList.isEmpty();
        if (empty) {
            throw new IllegalArgumentException(
                    String.format(NO_STEPS_IN_BUG, taskId));
        }
    }
}
