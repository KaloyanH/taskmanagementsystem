package com.telerikacademy.oop.team.exceptions;

public class InvalidUserInputException extends RuntimeException {

    public InvalidUserInputException(String message) {
        super(message);
    }

}
