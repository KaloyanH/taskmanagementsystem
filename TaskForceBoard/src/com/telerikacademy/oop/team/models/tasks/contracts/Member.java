package com.telerikacademy.oop.team.models.tasks.contracts;

import java.util.List;

public interface Member {

    void setTeam(String team);

    String getMemberName();

    List<Task> getMemberTaskList();

    String getMemberHistory();

    String getTeam();

    void addToMemberHistory(String activityToAdd);

    void addComment(Comment commentToAdd, Task taskToAddComment);
}
