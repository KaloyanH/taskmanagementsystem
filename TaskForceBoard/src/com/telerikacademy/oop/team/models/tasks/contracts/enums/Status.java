package com.telerikacademy.oop.team.models.tasks.contracts.enums;

public enum Status {

    ACTIVE,
    FIXED,


    NOTDONE,
    INPROGRESS,
    DONE,



    NEW,
    UNSCHEDULED,
    SCHEDULED;


    @Override
    public String toString() {
        switch (this) {
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
            case NOTDONE:
                return "Not done";
            case INPROGRESS:
                return "In progress";
            case UNSCHEDULED:
                return "Unscheduled";
            case SCHEDULED:
                return "Scheduled";
            case DONE:
                return "Done";
            case NEW:
                return "New";
            default:
                return "";
        }
    }
}
