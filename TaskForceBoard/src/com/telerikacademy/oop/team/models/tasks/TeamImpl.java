package com.telerikacademy.oop.team.models.tasks;

import com.telerikacademy.oop.team.models.tasks.contracts.Board;
import com.telerikacademy.oop.team.models.tasks.contracts.History;
import com.telerikacademy.oop.team.models.tasks.contracts.Member;
import com.telerikacademy.oop.team.models.tasks.contracts.Team;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;


import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

public class TeamImpl implements Team {

    public static final String MEMBER_ALREADY_EXISTS = "There's already %s";

    private String name;
    private List<Member> memberList = new ArrayList<>();
    private List<Board> boardList = new ArrayList<>();
    private List<History> teamHistory = new ArrayList<>();

    public TeamImpl(String name) {
        setName(name);
    }

    private void setName(String name) {
        ValidationHelper.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH,
                NAME_LENGTH_ERROR_MESSAGE);
        this.name = name;
    }

    @Override
    public String getTeamName() {
        return name;
    }

    @Override
    public List<Board> getBoardList() {
        return new ArrayList<>(boardList);
    }

    @Override
    public String getTeamHistory() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format(TEAM_HEADER, getTeamName())).append(System.lineSeparator());
        for (History history : teamHistory) {
            builder.append(history.viewInfo());
            builder.append(System.lineSeparator());
        }

        return builder.toString();
    }

    public List<Member> getMemberList() {
        return new ArrayList<>(memberList);
    }

    @Override
    public void addBoard(Board board) {
        boardList.add(board);
    }

    @Override
    public void addToTeamHistory(String activityToAdd) {
        teamHistory.add(new HistoryImpl(activityToAdd));
    }

    public void addMember(Member member) {
        String nameOfMemberToAdd = member.getMemberName();
        for (Member member1 : memberList) {
            if (member1.getMemberName().equals(nameOfMemberToAdd)) {
                throw new IllegalArgumentException(String.format(MEMBER_ALREADY_EXISTS, nameOfMemberToAdd));
            }
        }
        memberList.add(member);
    }

    @Override
    public boolean containsMember(String name) {
        for (Member member : memberList) {
            if (member.getMemberName().equals(name)) {
                return true;
            }
        }
        return false;
    }

}
