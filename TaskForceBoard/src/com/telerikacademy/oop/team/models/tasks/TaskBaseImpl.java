package com.telerikacademy.oop.team.models.tasks;

import com.telerikacademy.oop.team.models.tasks.contracts.Comment;
import com.telerikacademy.oop.team.models.tasks.contracts.History;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.models.tasks.contracts.enums.Status;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public abstract class TaskBaseImpl implements Task {


    private final int id;
    private LocalDate dueDate;
    private String title;
    private String description;
    private Status status;
    private List<Comment> comments = new ArrayList<>();
    private List<History> taskHistory = new ArrayList<>();

    public TaskBaseImpl(int id, String title, String description, Status status) {

        this.dueDate = LocalDate.now();
        setTitle(title);
        setDescription(description);
        this.status = status;
        this.id = id;

    }

    private void setTitle(String title) {

        ValidationHelper.validateStringLength(title, TITLE_LENGTH_MIN_SYMBOLS, TITLE_LENGTH_MAX_SYMBOLS, TITLE_LENGTH_ERROR);
        this.title = title;
        addToTaskHistory(String.format("The title was set to %s", getTittle()));

    }

    private void setDescription(String description) {

        ValidationHelper.validateStringLength(description, DESCRIPTION_LENGTH_MIN_SYMBOLS, DESCRIPTION_LENGTH_MAX_SYMBOLS, DESCRIPTION_LENGTH_ERROR);
        this.description = description;
        addToTaskHistory(String.format("The description was set to %s", getDescription()));
    }

    protected void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public abstract TaskType getType();

    @Override
    public String getTaskHistory() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(TASK_HEADER, getType())).append(System.lineSeparator());
        for (History task : taskHistory) {
            builder.append(task.viewInfo()).append(System.lineSeparator());
        }

        return builder.toString();
    }

    @Override
    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Status getStatus() {
        return status;
    }

    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    public abstract void advanceStatus();

    public abstract void revertStatus();

    public void addComment(Comment comment) {
        this.comments.add(comment);
    }

    public void addToTaskHistory(String event) {
        taskHistory.add(new HistoryImpl(event));
    }

}
