package com.telerikacademy.oop.team.models.tasks;

import com.telerikacademy.oop.team.models.tasks.contracts.Board;
import com.telerikacademy.oop.team.models.tasks.contracts.History;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

public class BoardImpl implements Board {

    private String name;
    private List<Task> tasks = new ArrayList<>();
    private List<History> boardHistory = new ArrayList<>();


    public BoardImpl(String name) {
        setName(name);
    }

    private void setName(String name) {
        ValidationHelper.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH,
                NAME_LENGTH_ERROR_MESSAGE);
        this.name = name;

    }

    @Override
    public List<Task> getTasksList() {
        return new ArrayList<>(tasks);
    }

    @Override
    public String getBoardHistory() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(BOARD_HEADER, getName())).append(System.lineSeparator());
        for (History history : boardHistory) {
            builder.append(history.viewInfo());
            builder.append(System.lineSeparator());
        }

        return builder.toString();
    }

    public String getName() {
        return name;
    }

    @Override
    public void addToBoardHistory(String activityToAdd) {
        boardHistory.add(new HistoryImpl(activityToAdd));
    }

}
