package com.telerikacademy.oop.team.models.tasks;

import static java.lang.String.format;

public class ModelConstants {
    public static final int NAME_MIN_LENGTH = 5;
    public static final int NAME_MAX_LENGTH = 15;
    public static final String NAME_LENGTH_ERROR_MESSAGE =
            String.format("Team name must be between %d and %d characters.",
                    NAME_MIN_LENGTH, NAME_MAX_LENGTH);
    public static final String MEMBER_LENGTH_NAME_ERROR_MESSAGE =
            String.format("Member name must be between %d and %d characters.",
                    NAME_MIN_LENGTH, NAME_MAX_LENGTH);
    public static final String ASSIGNEE_NAME_LENGTH_ERROR_MESSAGE =
            String.format("Assignee name must be between %d and %d characters.",
                    NAME_MIN_LENGTH, NAME_MAX_LENGTH);


    public static final int CONTENT_LEN_MIN = 3;
    public static final int CONTENT_LEN_MAX = 200;
    public static final String CONTENT_LEN_ERR = format(
            "Content must be between %d and %d characters long!",
            CONTENT_LEN_MIN,
            CONTENT_LEN_MAX);

    public static final int TITLE_LENGTH_MIN_SYMBOLS = 10;
    public static final int TITLE_LENGTH_MAX_SYMBOLS = 50;
    public static final int DESCRIPTION_LENGTH_MIN_SYMBOLS = 10;
    public static final int DESCRIPTION_LENGTH_MAX_SYMBOLS = 500;
    public static String CURRENT_TASK_TITTLE = "Task";
    public static String CURRENT_DESCRIPTION_TITTLE = "Description";
    public static final String TITLE_LENGTH_ERROR = String.format("Title must be between %d and %d characters long.", TITLE_LENGTH_MIN_SYMBOLS, TITLE_LENGTH_MAX_SYMBOLS);
    public static final String DESCRIPTION_LENGTH_ERROR = String.format("%s must be between %d and %d characters long.",
            CURRENT_DESCRIPTION_TITTLE, DESCRIPTION_LENGTH_MIN_SYMBOLS, DESCRIPTION_LENGTH_MAX_SYMBOLS);

    public static final String HEADER_FOOTER = "################";
    public static final String TEAMS_HEADER = "---TASKFORCE TEAMS---";
    public static final String USERS_HEADER = "---TASKFORCE USERS---";
    public static final String TEAM_MEMBERS_HEADER = "---TEAM '%s' MEMBERS---";
    public static final String TEAM_BOARDS_HEADER = "---TEAM '%s' BOARDS---";
    public static final String TEAM_HEADER = "---TEAM activity for '%s'---";
    public static final String BOARD_HEADER = "---BOARD activity for '%s'---";
    public static final String MEMBER_HEADER = "---MEMBER activity for '%s'---";
    public static final String TASK_HEADER = "---Task history for '%s'---";

    public final static String COMMENTS_HEADER = "--COMMENTS--";
    public static final String NO_TEAMS = "There are no registered teams.";
    public static final String NO_MEMBERS = "There are no registered members.";
    public static final String NO_BOARDS = "There are no registered boards.";
    public static final String NO_TASKS = "There are no registered tasks.";

    public static final String INVALID_COMMAND = "There's no such command for the %s!";
    public static final String INVALID_INPUT_MESSAGE = "Invalid input. Expected a number.";

    public static final String MEMBER_DOES_NOT_EXIST_IN_TEAM = "Member '%s' does not exist in any team and you can't assign him tasks!";

    public static final String DEFAULT_ASSIGNEE = "Unassigned";

}
