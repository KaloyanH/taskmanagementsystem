package com.telerikacademy.oop.team.models.tasks;


import com.telerikacademy.oop.team.models.tasks.contracts.Comment;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

public class CommentImpl implements Comment {


    private String content;
    private String author;

    public CommentImpl(String content, String author) {
        setContent(content);
        setAuthor(author);
    }

    private void setContent(String content) {
        ValidationHelper.validateValueInRange(content.length(), CONTENT_LEN_MIN, CONTENT_LEN_MAX, CONTENT_LEN_ERR);
        this.content = content;
    }

    private void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return COMMENTS_HEADER + System.lineSeparator() +
                String.format("%s%n", getContent()) +
                String.format("User: %s%n", getAuthor()) +
                "----------\n";
    }

}
