package com.telerikacademy.oop.team.models.tasks;

import com.telerikacademy.oop.team.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.team.models.tasks.contracts.Bug;
import com.telerikacademy.oop.team.models.tasks.contracts.enums.Priority;
import com.telerikacademy.oop.team.models.tasks.contracts.enums.Severity;
import com.telerikacademy.oop.team.models.tasks.contracts.enums.Status;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;
import static com.telerikacademy.oop.team.models.tasks.contracts.enums.Status.ACTIVE;
import static com.telerikacademy.oop.team.models.tasks.contracts.enums.Status.FIXED;

public class BugImpl extends TaskBaseImpl implements Bug {

    private static final TaskType type = TaskType.BUG;
    private List<String> steps = new ArrayList<>();
    private Priority priority;
    private Severity severity;
    private String assignee;


    public BugImpl(int id, String title, String description) {

        super(id, title, description, Status.ACTIVE);
        setPriority(Priority.HIGH);
        setSeverity(Severity.MINOR);
        setSteps(steps);
        this.assignee = DEFAULT_ASSIGNEE;
        this.addToTaskHistory(String.format("Priority of bug is currently %s", getPriority()));
        this.addToTaskHistory(String.format("Severity of bug is currently %s", getSeverity()));
        this.addToTaskHistory(String.format("Assignee is currently %s", getAssignee()));

    }

    @Override
    public void setAssignee(String assignee) {
        ValidationHelper.validateStringLength(assignee, NAME_MIN_LENGTH, NAME_MAX_LENGTH,
                ASSIGNEE_NAME_LENGTH_ERROR_MESSAGE);
        String currentAssignee = getAssignee();
        this.assignee = assignee;
        this.addToTaskHistory(String.format("Assignee changed from %s to %s", currentAssignee, assignee));
    }

    private void setSteps(List<String> steps) {
        this.steps = steps;
    }

    private void setPriority(Priority priority) {
        this.priority = priority;
    }

    private void setSeverity(Severity severity) {
        this.severity = severity;
    }

    @Override
    public List<String> getStepsList() {

        return new ArrayList<>(steps);
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public Severity getSeverity() {
        return severity;
    }

    @Override
    public String getAssignee() {
        return assignee;
    }

    @Override
    public TaskType getType() {
        return type;
    }

    public String getTittle() {
        return super.getTitle();
    }

    @Override
    public void advanceStatus() {
        switch (this.getStatus()) {
            case ACTIVE:
                super.setStatus(FIXED);
                String newInfo = ("Active status set to " + super.getStatus());
                this.addToTaskHistory(newInfo);
                break;
            case FIXED:
                String newInfo2 = ("Status of bug already at Fixed, can't advance any further");
                this.addToTaskHistory(newInfo2);
                throw new InvalidUserInputException(newInfo2);
            default:
                throw new IllegalStateException("There's no such bug status - " + super.getStatus());
        }
    }

    @Override
    public void revertStatus() {
        switch (this.getStatus()) {
            case ACTIVE:
                String newInfo1 = ("Status of bug already at active, can't revert any further");
                this.addToTaskHistory(newInfo1);
                throw new InvalidUserInputException(newInfo1);
            case FIXED:
                super.setStatus(ACTIVE);
                String newInfo2 = ("Fixed status set to " + super.getStatus());
                this.addToTaskHistory(newInfo2);
                break;
            default:
                throw new IllegalStateException("There's no such bug status - " + super.getStatus());
        }
    }

    @Override
    public void advancePriority() {
        switch (this.getPriority()) {
            case HIGH:
                setPriority(Priority.MEDIUM);
                String newInfo = ("High priority set to " + this.priority);
                this.addToTaskHistory(newInfo);
                break;
            case MEDIUM:
                setPriority(Priority.LOW);
                String newInfo2 = ("Medium priority set to " + this.priority);
                this.addToTaskHistory(newInfo2);
                break;
            case LOW:
                String newInfo3 = ("Priority already at Low can't advance any further");
                this.addToTaskHistory(newInfo3);
                throw new InvalidUserInputException(newInfo3);
            default:
                throw new IllegalStateException("There's no such priority");
        }
    }

    @Override
    public void revertPriority() {
        switch (this.getPriority()) {
            case HIGH:
                String newInfo3 = ("Priority already at high can't revert anymore");
                this.addToTaskHistory(newInfo3);
                throw new InvalidUserInputException(newInfo3);
            case MEDIUM:
                setPriority(Priority.HIGH);
                String newInfo2 = ("Medium priority set to " + this.priority);
                this.addToTaskHistory(newInfo2);
                break;
            case LOW:
                setPriority(Priority.MEDIUM);
                String newInfo = ("Low priority set to " + this.priority);
                this.addToTaskHistory(newInfo);
                break;
            default:
                throw new IllegalStateException("There's no such priority");
        }
    }

    @Override
    public void revertSeverity() {
        switch (this.getSeverity()) {
            case CRITICAL:
                setSeverity(Severity.MAJOR);
                String newInfo = ("Critical severity set to " + this.severity);
                this.addToTaskHistory(newInfo);
                break;
            case MAJOR:
                setSeverity(Severity.MINOR);
                String newInfo2 = ("Major severity set to " + this.severity);
                this.addToTaskHistory(newInfo2);
                break;
            case MINOR:
                String newInfo3 = ("Severity already at minor can't advance any further");
                this.addToTaskHistory(newInfo3);
                throw new InvalidUserInputException(newInfo3);
            default:
                throw new IllegalStateException("There's no such severity");
        }
    }

    @Override
    public void advanceSeverity() {
        switch (this.getSeverity()) {
            case CRITICAL:
                String newInfo3 = ("Severity already at critical can't revert anymore");
                this.addToTaskHistory(newInfo3);
                throw new InvalidUserInputException(newInfo3);
            case MAJOR:
                setSeverity(Severity.CRITICAL);
                String newInfo2 = ("Major severity set to " + this.severity);
                this.addToTaskHistory(newInfo2);
                break;
            case MINOR:
                setSeverity(Severity.MAJOR);
                String newInfo = ("Minor severity set to " + this.severity);
                this.addToTaskHistory(newInfo);
                break;
            default:
                throw new IllegalStateException("There's no such severity");
        }
    }

    @Override
    public void addToStepsList(String stepToAdd) {
        steps.add(stepToAdd);
    }

    @Override
    public void addToTaskHistory(String taskToAdd) {
        super.addToTaskHistory(taskToAdd);
    }

}
