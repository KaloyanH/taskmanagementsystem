package com.telerikacademy.oop.team.models.tasks.contracts.enums;

public enum Priority {
    HIGH,
    LOW,
    MEDIUM;



    @Override
    public String toString() {
        switch (this) {
            case HIGH:
                return "High";
            case MEDIUM:
                return "Medium";
            case LOW:
                return "Low";
            default:
                return "No such priority";
        }
    }

}
