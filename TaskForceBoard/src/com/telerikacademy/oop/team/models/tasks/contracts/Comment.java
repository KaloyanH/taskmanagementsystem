package com.telerikacademy.oop.team.models.tasks.contracts;

public interface Comment {

    String getContent();

    String getAuthor();

}
