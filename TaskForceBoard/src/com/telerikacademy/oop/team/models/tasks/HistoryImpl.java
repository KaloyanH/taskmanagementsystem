package com.telerikacademy.oop.team.models.tasks;

import com.telerikacademy.oop.team.models.tasks.contracts.History;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HistoryImpl implements History {
    public static final String NULL_DESCRIPTION = "Description cannot be null";
    private String description;
    private LocalDateTime timestamp;


    public HistoryImpl(String description) {
        validateDescription(description);
        this.description = description;
        this.timestamp = LocalDateTime.now();

    }

    private void validateDescription(String description) {
        if (description == null) {
            throw new IllegalArgumentException(NULL_DESCRIPTION);
        }
    }

    @Override
    public String viewInfo() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-LLLL-yyyy HH:mm:ss");
        String formatDateTime = timestamp.format(dateTimeFormatter);

        return String.format("[%s] %s", formatDateTime, description);
    }


}
