package com.telerikacademy.oop.team.models.tasks.contracts;

import com.telerikacademy.oop.team.models.tasks.contracts.enums.Priority;
import com.telerikacademy.oop.team.models.tasks.contracts.enums.Size;

public interface Story extends Task {

    void setAssignee(String assignee);

    Priority getPriority();

    Size getSize();

    String getAssignee();

    void advancePriority();

    void revertPriority();

    void revertSize();

    void advanceSize();

}