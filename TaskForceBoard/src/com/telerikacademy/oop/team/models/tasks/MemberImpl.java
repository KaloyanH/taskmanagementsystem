package com.telerikacademy.oop.team.models.tasks;

import com.telerikacademy.oop.team.models.tasks.contracts.Comment;
import com.telerikacademy.oop.team.models.tasks.contracts.History;
import com.telerikacademy.oop.team.models.tasks.contracts.Member;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;


import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

public class MemberImpl implements Member {

    private String name;
    private String team;

    private List<Task> memberTaskList = new ArrayList<>();
    private List<History> memberHistory = new ArrayList<>();

    public MemberImpl(String name) {
        setName(name);
    }

    public void setTeam(String team) {
        this.team = team;
    }

    private void setName(String name) {
        ValidationHelper.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH,
                MEMBER_LENGTH_NAME_ERROR_MESSAGE);
        this.name = name;

    }

    @Override
    public String getMemberName() {
        return name;
    }

    @Override
    public String getTeam() {
        return team;
    }

    @Override
    public List<Task> getMemberTaskList() {
        return memberTaskList;
    }

    @Override
    public String getMemberHistory() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format(MEMBER_HEADER, getMemberName())).append(System.lineSeparator());
        for (History history : memberHistory) {
            stringBuilder.append(history.viewInfo());
            stringBuilder.append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }

    @Override
    public void addComment(Comment commentToAdd, Task taskToAddComment) {
        taskToAddComment.addComment(commentToAdd);
    }

    @Override
    public void addToMemberHistory(String activityToAdd) {
        memberHistory.add(new HistoryImpl(activityToAdd));
    }

}
