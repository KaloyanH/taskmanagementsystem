package com.telerikacademy.oop.team.models.tasks.contracts;

import java.util.List;

public interface Board {

    String getName();

    String getBoardHistory();

    List<Task> getTasksList();

    void addToBoardHistory(String activityToAdd);

}
