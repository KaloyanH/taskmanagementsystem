package com.telerikacademy.oop.team.models.tasks.contracts;

public interface FeedBack extends Task {
    
    int getRating();

    void changeRating(int rating);
}