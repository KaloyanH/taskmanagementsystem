package com.telerikacademy.oop.team.models.tasks.contracts.enums;

public enum Severity {

    CRITICAL,
    MAJOR,
    MINOR;


    @Override
    public String toString() {
        switch (this) {
            case CRITICAL:
                return "Critical";
            case MAJOR:
                return "Major";
            case MINOR:
                return "Minor";
            default:
                return "No such severity";
        }
    }
}
