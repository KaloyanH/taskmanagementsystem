package com.telerikacademy.oop.team.models.tasks.contracts;

import com.telerikacademy.oop.team.models.tasks.contracts.enums.Priority;
import com.telerikacademy.oop.team.models.tasks.contracts.enums.Severity;

import java.util.List;

public interface Bug extends Task {

    void setAssignee(String assignee);

    List<String> getStepsList();

    void addToStepsList(String stepToAdd);

    Priority getPriority();

    Severity getSeverity();

    String getAssignee();

    void advancePriority();

    void revertPriority();

    void revertSeverity();

    void advanceSeverity();


}