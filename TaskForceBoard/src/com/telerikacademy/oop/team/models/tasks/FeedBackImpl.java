package com.telerikacademy.oop.team.models.tasks;


import com.telerikacademy.oop.team.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.team.models.tasks.contracts.FeedBack;
import com.telerikacademy.oop.team.models.tasks.contracts.enums.Status;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import static com.telerikacademy.oop.team.models.tasks.contracts.enums.Status.*;

public class FeedBackImpl extends TaskBaseImpl implements FeedBack {

    private static final TaskType type = TaskType.FEEDBACK;
    private int rating;

    public FeedBackImpl(int id, String title, String description, int rating) {

        super(id, title, description, Status.NEW);
        setRating(rating);

    }

    private void setRating(int rating) {

        ValidationHelper.validateIntegerNegative(rating);
        this.rating = rating;
        addToTaskHistory(String.format("Rating set to %d", getRating()));

    }

    public String getTittle() {
        return super.getTitle();
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public void changeRating(int rating) {
        this.rating = rating;
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public TaskType getType() {
        return type;
    }

    @Override
    public void advanceStatus() {
        switch (this.getStatus()) {
            case NEW:
                super.setStatus(UNSCHEDULED);
                String newInfo = ("New status set to " + super.getStatus());
                addToTaskHistory(newInfo);
                break;
            case UNSCHEDULED:
                super.setStatus(SCHEDULED);
                String newInfo2 = ("Unscheduled status set to " + super.getStatus());
                addToTaskHistory(newInfo2);
                break;
            case SCHEDULED:
                super.setStatus(DONE);
                String newInfo3 = ("Scheduled status set to " + super.getStatus());
                addToTaskHistory(newInfo3);
                break;
            case DONE:
                String newInfo4 = ("Status of feedback already at Done, can't advance any further");
                addToTaskHistory(newInfo4);
                throw new InvalidUserInputException(newInfo4);
            default:
                throw new IllegalStateException("There's no such feedback status - " + super.getStatus());
        }
    }

    @Override
    public void revertStatus() {
        switch (this.getStatus()) {
            case NEW:
                String newInfo4 = ("Status of feedback is already at new, can't revert any further");
                addToTaskHistory(newInfo4);
                throw new InvalidUserInputException(newInfo4);
            case UNSCHEDULED:
                super.setStatus(NEW);
                String newInfo3 = ("Unscheduled status set to " + super.getStatus());
                addToTaskHistory(newInfo3);
                break;
            case SCHEDULED:
                super.setStatus(UNSCHEDULED);
                String newInfo2 = ("Scheduled status set to " + super.getStatus());
                addToTaskHistory(newInfo2);
                break;
            case DONE:
                super.setStatus(SCHEDULED);
                String newInfo = ("Done status set to " + super.getStatus());
                addToTaskHistory(newInfo);
                break;
            default:
                throw new IllegalStateException("There's no such feedback status - " + super.getStatus());
        }
    }


}