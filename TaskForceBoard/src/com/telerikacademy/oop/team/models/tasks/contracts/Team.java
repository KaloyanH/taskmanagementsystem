package com.telerikacademy.oop.team.models.tasks.contracts;

import java.util.List;

public interface Team {

    String getTeamName();

    List<Member> getMemberList();

    List<Board> getBoardList();

    void addMember(Member member);

    void addBoard(Board board);

    void addToTeamHistory(String activityToAdd);

    boolean containsMember(String name);

    String getTeamHistory();

}
