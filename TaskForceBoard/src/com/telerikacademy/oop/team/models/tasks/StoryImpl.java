package com.telerikacademy.oop.team.models.tasks;

import com.telerikacademy.oop.team.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.team.models.tasks.contracts.Story;
import com.telerikacademy.oop.team.models.tasks.contracts.enums.Priority;
import com.telerikacademy.oop.team.models.tasks.contracts.enums.Size;
import com.telerikacademy.oop.team.models.tasks.contracts.enums.Status;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.utils.ValidationHelper;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;
import static com.telerikacademy.oop.team.models.tasks.contracts.enums.Status.*;

public class StoryImpl extends TaskBaseImpl implements Story {

    private static final TaskType type = TaskType.STORY;
    private Priority priority;
    private Size size;
    private String assignee;


    public StoryImpl(int id, String title, String description) {
        super(id, title, description, Status.NOTDONE);
        setPriority(Priority.HIGH);
        setSize(Size.LARGE);
        this.assignee = DEFAULT_ASSIGNEE;
        addToTaskHistory(String.format("Priority of story is currently %s", getPriority()));
        addToTaskHistory(String.format("Size of story is currently %s", getSize()));
        addToTaskHistory(String.format("Assignee is currently %s", getAssignee()));
    }

    private void setPriority(Priority priority) {
        this.priority = priority;
    }

    private void setSize(Size size) {
        this.size = size;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public String getAssignee() {
        return assignee;
    }


    @Override
    public String getTittle() {
        return getTitle();
    }

    @Override
    public TaskType getType() {
        return type;
    }

    @Override
    public void setAssignee(String assignee) {

        ValidationHelper.validateStringLength(assignee, NAME_MIN_LENGTH, NAME_MAX_LENGTH,
                ASSIGNEE_NAME_LENGTH_ERROR_MESSAGE);
        String currentAssignee = getAssignee();
        this.assignee = assignee;
        addToTaskHistory(String.format("Assignee changed from %s to %s", currentAssignee, assignee));
    }

    @Override
    public void advancePriority() {
        switch (this.getPriority()) {
            case HIGH:
                setPriority(Priority.MEDIUM);

                String newInfo = ("High priority set to " + this.priority);
                addToTaskHistory(newInfo);
                break;
            case MEDIUM:
                setPriority(Priority.LOW);
                String newInfo2 = ("Medium priority set to " + this.priority);
                addToTaskHistory(newInfo2);
                break;
            case LOW:
                String newInfo3 = ("Priority already at low can't advance any further");
                addToTaskHistory(newInfo3);
                throw new InvalidUserInputException(newInfo3);
            default:
                throw new IllegalStateException("There's no such priority");
        }
    }

    @Override
    public void revertPriority() {
        switch (this.getPriority()) {
            case HIGH:
                String newInfo3 = ("Priority already at high can't revert anymore");
                addToTaskHistory(newInfo3);
                throw new InvalidUserInputException(newInfo3);
            case MEDIUM:
                setPriority(Priority.HIGH);
                String newInfo2 = ("Medium priority set to " + this.priority);
                addToTaskHistory(newInfo2);
                break;
            case LOW:
                setPriority(Priority.MEDIUM);
                String newInfo = ("Low priority set to " + this.priority);
                addToTaskHistory(newInfo);
                break;
            default:
                throw new IllegalStateException("There's no such priority");
        }
    }

    @Override
    public void revertSize() {
        switch (this.getSize()) {
            case LARGE:
                setSize(Size.MEDIUM);
                String newInfo = ("Large size set to " + this.size);
                addToTaskHistory(newInfo);
                break;
            case MEDIUM:
                setSize(Size.SMALL);
                String newInfo2 = ("Medium size set to " + this.size);
                addToTaskHistory(newInfo2);
                break;
            case SMALL:
                String newInfo3 = ("Size already at small can't revert any further");
                addToTaskHistory(newInfo3);
                throw new InvalidUserInputException(newInfo3);
            default:
                throw new IllegalStateException("There's no such size");
        }
    }

    @Override
    public void advanceSize() {
        switch (this.getSize()) {
            case LARGE:
                String newInfo3 = ("Size already at large can't advance anymore");
                addToTaskHistory(newInfo3);
                throw new InvalidUserInputException(newInfo3);
            case MEDIUM:
                setSize(Size.LARGE);
                String newInfo2 = ("Medium size set to " + this.size);
                addToTaskHistory(newInfo2);
                break;
            case SMALL:
                setSize(Size.MEDIUM);
                String newInfo = ("Small size set to " + this.size);
                addToTaskHistory(newInfo);
                break;
            default:
                throw new IllegalStateException("There's no such size");
        }
    }

    @Override
    public void advanceStatus() {
        switch (this.getStatus()) {
            case NOTDONE:
                super.setStatus(INPROGRESS);
                String newInfo = ("Not done status set to " + super.getStatus());
                addToTaskHistory(newInfo);
                break;
            case INPROGRESS:
                super.setStatus(DONE);
                String newInfo2 = ("In Progress status set to " + super.getStatus());
                addToTaskHistory(newInfo2);
                break;
            case DONE:
                String newInfo3 = ("Status of story already at Done, can't advance any further");
                addToTaskHistory(newInfo3);
                throw new InvalidUserInputException(newInfo3);
            default:
                throw new IllegalStateException("There's no such story status - " + super.getStatus());
        }
    }

    @Override
    public void revertStatus() {
        switch (this.getStatus()) {
            case NOTDONE:
                String newInfo3 = ("Status of story is already at Not done, can't revert any further");
                addToTaskHistory(newInfo3);
                throw new InvalidUserInputException(newInfo3);
            case INPROGRESS:
                super.setStatus(NOTDONE);
                String newInfo2 = ("In Progress status set to " + super.getStatus());
                addToTaskHistory(newInfo2);
                break;
            case DONE:
                super.setStatus(INPROGRESS);
                String newInfo = ("Done status set to " + super.getStatus());
                addToTaskHistory(newInfo);
                break;
            default:
                throw new IllegalStateException("There's no such story status - " + super.getStatus());
        }
    }

    @Override
    public void addToTaskHistory(String taskToAdd) {
        super.addToTaskHistory(taskToAdd);
    }


}