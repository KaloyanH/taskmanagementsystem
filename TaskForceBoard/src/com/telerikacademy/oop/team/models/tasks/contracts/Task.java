package com.telerikacademy.oop.team.models.tasks.contracts;

import com.telerikacademy.oop.team.models.contracts.Identifiable;
import com.telerikacademy.oop.team.models.tasks.enums.TaskType;
import com.telerikacademy.oop.team.models.tasks.contracts.enums.Status;

import java.util.List;

public interface Task extends Identifiable {

    void addToTaskHistory(String taskToAdd);

    String getTittle();

    TaskType getType();

    String getDescription();

    Status getStatus();

    List<Comment> getComments();

    String getTaskHistory();

    void addComment(Comment commentToAdd);

    void advanceStatus();

    void revertStatus();

}