package com.telerikacademy.oop.team.models.contracts;

public interface Identifiable {

    int getId();

}
