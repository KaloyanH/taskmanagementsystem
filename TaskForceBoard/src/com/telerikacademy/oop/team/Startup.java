package com.telerikacademy.oop.team;

import com.telerikacademy.oop.team.core.TaskEngineImpl;


public class Startup {

    public static void main(String[] args) {
        
        TaskEngineImpl engine = new TaskEngineImpl();
        engine.start();
        
    }

}
