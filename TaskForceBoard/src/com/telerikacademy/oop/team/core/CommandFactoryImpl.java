package com.telerikacademy.oop.team.core;

import com.telerikacademy.oop.team.commands.contracts.Command;
import com.telerikacademy.oop.team.commands.creation.*;
import com.telerikacademy.oop.team.commands.enums.CommandType;
import com.telerikacademy.oop.team.core.contracts.CommandFactory;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.utils.ParsingHelpers;

public class CommandFactoryImpl implements CommandFactory {

    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    public Command createCommandFromCommandName(String commandName, TaskRepository taskRepository) {
        CommandType commandType = ParsingHelpers.tryParseEnum(commandName, CommandType.class);

        switch (commandType) {
            case CREATEMEMBER:
                return new CreateMemberCommand(taskRepository);
            case CREATETEAM:
                return new CreateTeamCommand(taskRepository);
            case CREATEBOARD:
                return new CreateBoardCommand(taskRepository);
            case CREATEBUG:
                return new CreateBugCommand(taskRepository);
            case CREATESTORY:
                return new CreateStoryCommand(taskRepository);
            case CREATEFEEDBACK:
                return new CreateFeedBackCommand(taskRepository);
            case SHOWALLUSERS:
                return new ShowAllUsersCommand(taskRepository);
            case SHOWUSERSACTIVITY:
                return new ShowUsersActivityCommand(taskRepository);
            case SHOWALLTEAMS:
                return new ShowAllTeamsCommand(taskRepository);
            case SHOWTEAMSACTIVITY:
                return new ShowTeamsActivityCommand(taskRepository);
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembersCommand(taskRepository);
            case SHOWALLSTEPS:
                return new ShowAllStepsOfBug(taskRepository);
            case SHOWALLBOARDS:
                return new ShowAllBoardsCommand(taskRepository);
            case SHOWBOARDSACTIVITY:
                return new ShowBoardsActivityCommand(taskRepository);
            case SHOWCOMMENTS:
                return new ShowCommentsCommand(taskRepository);
            case SHOWTASKSACTIVITY:
                return new ShowTasksActivityCommand(taskRepository);
            case ADDMEMBERTOTEAM:
                return new AddMemberToTeamCommand(taskRepository);
            case ADDCOMMENTTOTASK:
                return new AddCommentToTaskCommand(taskRepository);
            case ADDSTEPS:
                return new AddStepsToBugCommand(taskRepository);
            case ADDTASKTOBOARD:
                return new AddTaskToBoardCommand(taskRepository);
            case CHANGEPRIORITY:
                return new ChangePriorityCommand(taskRepository);
            case CHANGESEVERITY:
                return new ChangeSeverityCommand(taskRepository);
            case CHANGESTATUS:
                return new ChangeStatusCommand(taskRepository);
            case CHANGESIZE:
                return new ChangeSizeCommand(taskRepository);
            case CHANGERATING:
                return new ChangeRatingCommand(taskRepository);
            case ASSIGNTASK:
                return new AssignTaskCommand(taskRepository);
            case UNASSIGNTASK:
                return new UnAssignTaskCommand(taskRepository);
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandName));
        }
    }

}