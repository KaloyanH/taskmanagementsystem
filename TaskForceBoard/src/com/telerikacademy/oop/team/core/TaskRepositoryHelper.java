package com.telerikacademy.oop.team.core;

import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.models.contracts.Identifiable;
import com.telerikacademy.oop.team.models.tasks.contracts.Board;
import com.telerikacademy.oop.team.models.tasks.contracts.Member;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import com.telerikacademy.oop.team.models.tasks.contracts.Team;

import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

public class TaskRepositoryHelper {

    private static final String MEMBER_DOES_NOT_EXIST = "Member '%s' does not exist!";
    private static final String TEAM_DOES_NOT_EXIST = "Team '%s' does not exist!";
    private static final String BOARD_DOES_NOT_EXIST = "Board '%s' does not exist!";
    private static final String NO_RECORD = "No record with ID %d";


    private final TaskRepository taskRepository;

    public TaskRepositoryHelper(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public TaskRepository getTaskRepo() {
        return taskRepository;
    }


    public Member findMemberByName(String memberName) {
        return taskRepository.getMembers()
                .stream()
                .filter(member -> member.getMemberName()
                        .equals(memberName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(MEMBER_DOES_NOT_EXIST, memberName)));
    }

    public Team findTeamByName(String teamName) {
        return taskRepository.getTeams()
                .stream()
                .filter(team -> team.getTeamName()
                        .equals(teamName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(TEAM_DOES_NOT_EXIST, teamName)));
    }

    public Board findBoardByName(String boardName) {
        return taskRepository.getBoards()
                .stream()
                .filter(board -> board.getName()
                        .equals(boardName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(BOARD_DOES_NOT_EXIST, boardName)));
    }


    public <T extends Identifiable> T findElementById(List<T> elements, int id) {
        return elements
                .stream()
                .filter(elementId -> elementId.getId() == id)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(NO_RECORD, id)));
    }

    public boolean findMemberInTeam(String memberName) {
        for (Team team : taskRepository.getTeams()) {
            if (team.containsMember(memberName)) {
                return true;
            }
        }
        throw new IllegalArgumentException(String.format(MEMBER_DOES_NOT_EXIST_IN_TEAM, memberName));
    }

    public String getTeamHistory(String teamName) {
        StringBuilder builder = new StringBuilder();
        if (taskRepository.getTeams().size() == 0 && teamName.equals("")) {
            throw new IllegalArgumentException(NO_TEAMS);
        }

        for (Team team : taskRepository.getTeams()) {
            if (team.getTeamName().equals(teamName)) {
                builder.append(team.getTeamHistory());
            }
        }

        return builder.toString();
    }

    public String getMemberHistory(String memberName) {
        StringBuilder builder = new StringBuilder();
        if (taskRepository.getMembers().size() == 0 && memberName.equals("")) {
            throw new IllegalArgumentException(NO_MEMBERS);
        }
        for (Member member : taskRepository.getMembers()) {
            if (member.getMemberName().equals(memberName)) {
                builder.append(member.getMemberHistory());
            }
        }

        return builder.toString();
    }

    public String getBoardHistory(String boardName) {
        if (taskRepository.getBoards().size() == 0 && boardName.equals("")) {
            throw new IllegalArgumentException(NO_BOARDS);
        }
        StringBuilder builder = new StringBuilder();

        for (Board teamBoards : taskRepository.getBoards()) {
            if (teamBoards.getName().equals(boardName)) {
                builder.append(teamBoards.getBoardHistory());
            }
        }

        return builder.toString();
    }

    public String getTaskHistory(int idOfTask) {
        StringBuilder builder = new StringBuilder();
        if (taskRepository.getTasks().size() == 0) {
            throw new IllegalArgumentException(NO_TASKS);
        }

        for (Task event : taskRepository.getTasks()) {
            if (event.getId() == idOfTask) {
                builder.append(event.getTaskHistory()).append(System.lineSeparator());
            }
        }

        return builder.toString();
    }

}

