package com.telerikacademy.oop.team.core.contracts;

import com.telerikacademy.oop.team.models.tasks.contracts.*;


import java.util.List;

public interface TaskRepository {

    Member createMember(String memberName);

    List<Member> getMembers();

    Team createTeam(String teamName);

    List<Team> getTeams();

    Board createTeamBoard(String boardName);

    List<Board> getBoards();

    Bug createBug(String title, String description);

    Story createStory(String title, String description);

    FeedBack createFeedBack(String title, String description, int rating);

    Comment createComment(String content, String author);

    List<Task> getTasks();

    List<Story> getStory();

    List<Bug> getBug();

    List<FeedBack> getFeedback();

}