package com.telerikacademy.oop.team.core.contracts;

public interface Engine {

    void start();

}