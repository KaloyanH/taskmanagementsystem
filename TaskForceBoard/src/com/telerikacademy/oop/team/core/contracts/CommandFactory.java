package com.telerikacademy.oop.team.core.contracts;

import com.telerikacademy.oop.team.commands.contracts.Command;

public interface CommandFactory {

    Command createCommandFromCommandName(String commandTypeAsString, TaskRepository taskRepository);

}