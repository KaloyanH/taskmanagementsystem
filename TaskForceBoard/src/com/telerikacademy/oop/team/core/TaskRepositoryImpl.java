package com.telerikacademy.oop.team.core;

import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.team.models.tasks.*;
import com.telerikacademy.oop.team.models.tasks.contracts.*;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.NO_TASKS;

public class TaskRepositoryImpl implements TaskRepository {
    
    private static final String NO_BUGS = "There are no bugs.";
    private static final String NO_STORIES = "There are no stories.";
    private static final String NO_FEEDBACKS = "There are no feedbacks.";

    private final List<Team> teams = new ArrayList<>();
    private final List<Member> members = new ArrayList<>();
    private final List<Board> teamBoards = new ArrayList<>();
    private final List<Task> tasks = new ArrayList<>();
    private final List<Bug> bugs = new ArrayList<>();
    private final List<FeedBack> feedbacks = new ArrayList<>();
    private final List<Story> stories = new ArrayList<>();
    private int id;

    @Override
    public Team createTeam(String teamName) {
        Team team = new TeamImpl(teamName);
        teams.add(team);
        return team;
    }

    @Override
    public Member createMember(String memberName) {
        Member member = new MemberImpl(memberName);
        members.add(member);
        return member;
    }

    @Override
    public Board createTeamBoard(String boardName) {
        Board board = new BoardImpl(boardName);
        teamBoards.add(board);
        return board;
    }

    @Override
    public Bug createBug(String title, String description) {
        Bug bug = new BugImpl(++id, title, description);
        bugs.add(bug);
        tasks.add(bug);
        return bug;
    }

    @Override
    public Story createStory(String title, String description) {
        StoryImpl story = new StoryImpl(++id, title, description);
        stories.add(story);
        tasks.add(story);
        return story;
    }

    @Override
    public FeedBack createFeedBack(String title, String description, int rating) {
        FeedBack feedBack = new FeedBackImpl(++id, title, description, rating);
        feedbacks.add(feedBack);
        tasks.add(feedBack);
        return feedBack;
    }

    @Override
    public Comment createComment(String content, String author) {
        return new CommentImpl(content, author);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(teamBoards);
    }

    @Override
    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(members);
    }

    @Override
    public List<Task> getTasks() {
        if (tasks.size() == 0) {
            throw new InvalidUserInputException(NO_TASKS);
        }
        return new ArrayList<>(tasks);
    }

    @Override
    public List<Story> getStory() {

        if (stories.size() == 0) {
            throw new InvalidUserInputException(NO_STORIES);
        }
        return new ArrayList<>(stories);
    }

    @Override
    public List<Bug> getBug() {
        if (bugs.size() == 0) {
            throw new InvalidUserInputException(NO_BUGS);
        }
        return new ArrayList<>(bugs);
    }

    @Override
    public List<FeedBack> getFeedback() {
        if (feedbacks.size() == 0) {
            throw new InvalidUserInputException(NO_FEEDBACKS);
        }
        return new ArrayList<>(feedbacks);
    }
}