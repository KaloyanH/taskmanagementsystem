package com.telerikacademy.oop.team.utils;

import java.util.List;

public class ValidationHelper {


    public static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String NEGATIVE_DOUBLE_NUMBER = "Invalid number. %.2f should be positive";
    private static final String NEGATIVE_INTEGER_NUMBER = "Invalid number. %d should be positive";


    public static void validateValueInRange(double value, double min, double max, String errorMessage) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public static void validateIntRange(int value, int min, int max, String message) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateStringLength(String stringToValidate, int minLength, int maxLength, String errorMessage) {
        validateValueInRange(stringToValidate.length(), minLength, maxLength, errorMessage);
    }

    public static void validateArgumentsCount(List<String> list, int expectedArgumentsCount) {
        if (list.size() < expectedArgumentsCount || list.size() > expectedArgumentsCount) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, expectedArgumentsCount, list.size()));
        }
    }

    public static void validateIntegerNegative(int number) {
        if (number < 0) {
            throw new IllegalArgumentException(String.format(NEGATIVE_INTEGER_NUMBER, number));
        }
    }

//        public static void validateDoubleNegative(double number) {
//        if (number < 0) {
//            throw new IllegalArgumentException(String.format(NEGATIVE_DOUBLE_NUMBER, number));
//        }
//    }
}