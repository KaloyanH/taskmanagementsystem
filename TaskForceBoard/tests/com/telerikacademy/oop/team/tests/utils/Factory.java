package com.telerikacademy.oop.team.tests.utils;

import com.telerikacademy.oop.team.models.tasks.BoardImpl;
import com.telerikacademy.oop.team.models.tasks.MemberImpl;
import com.telerikacademy.oop.team.models.tasks.TeamImpl;
import com.telerikacademy.oop.team.models.tasks.contracts.Board;
import com.telerikacademy.oop.team.models.tasks.contracts.Member;
import com.telerikacademy.oop.team.models.tasks.contracts.Team;

import static com.telerikacademy.oop.team.tests.utils.TestData.Board.VALID_NAME;

public class Factory {

    public static Board createBoard() {
        return new BoardImpl(VALID_NAME);
    }

    public static Team createTeam() {
        return new TeamImpl(VALID_NAME);
    }

    public static Member createMember() {
        return new MemberImpl(VALID_NAME);
    }

}
