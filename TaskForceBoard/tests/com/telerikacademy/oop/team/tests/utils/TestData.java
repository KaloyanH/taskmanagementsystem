package com.telerikacademy.oop.team.tests.utils;


import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;

public class TestData {


    public static class Board {
        public static final String VALID_NAME = TestUtilities.initializeStringWithSize(NAME_MIN_LENGTH + 1);
    }

    public static class Team {
        public static final String VALID_NAME = TestUtilities.initializeStringWithSize(NAME_MIN_LENGTH + 1);
    }

    public static class Member {
        public static final String VALID_NAME = TestUtilities.initializeStringWithSize(NAME_MIN_LENGTH + 1);
    }

    public static class Bug {
        public static final String VALID_TITLE = TestUtilities.initializeStringWithSize(TITLE_LENGTH_MIN_SYMBOLS + 1);
        public static final String VALID_DESCRIPTION = TestUtilities.initializeStringWithSize(DESCRIPTION_LENGTH_MIN_SYMBOLS + 1);
    }

    public static class Feedback {
        public static final String VALID_TITLE = TestUtilities.initializeStringWithSize(TITLE_LENGTH_MIN_SYMBOLS + 1);
        public static final String VALID_DESCRIPTION = TestUtilities.initializeStringWithSize(DESCRIPTION_LENGTH_MIN_SYMBOLS + 1);
        public static final int RATING = 666;
    }

    public static class Story {
        public static final String VALID_TITLE = TestUtilities.initializeStringWithSize(TITLE_LENGTH_MIN_SYMBOLS + 1);
        public static final String VALID_DESCRIPTION = TestUtilities.initializeStringWithSize(DESCRIPTION_LENGTH_MIN_SYMBOLS + 1);
    }


}
