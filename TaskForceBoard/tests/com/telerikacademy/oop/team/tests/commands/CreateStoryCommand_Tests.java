package com.telerikacademy.oop.team.tests.commands;

import com.telerikacademy.oop.team.commands.contracts.Command;
import com.telerikacademy.oop.team.commands.creation.CreateFeedBackCommand;
import com.telerikacademy.oop.team.commands.creation.CreateStoryCommand;
import com.telerikacademy.oop.team.core.TaskRepositoryImpl;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static com.telerikacademy.oop.team.commands.creation.CreateStoryCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.team.tests.utils.TestData.Story.*;


/**
 * CreateStoryCommand arguments: {{Story title}} {{Story description}}
 */
public class CreateStoryCommand_Tests {

    private Command command;
    private TaskRepository taskRepository;

    @BeforeEach
    public void before() {
        this.taskRepository = new TaskRepositoryImpl();
        this.command = new CreateStoryCommand(taskRepository);
    }

    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        // Arrange
        List<String> arguments = TestUtilities.initializeListWithSize(argumentsCount);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }


    @Test
    public void execute_should_add_NewFeedBack_to_feedbackList_when_passedValidInput() {
        // Arrange

        List<String> arguments = List.of(VALID_TITLE, VALID_DESCRIPTION);

        // Act, Assert
        Assertions.assertAll(
                Assertions.assertDoesNotThrow(() -> command.execute(arguments)),
                () -> Assertions.assertFalse(taskRepository.getStory().isEmpty())
        );
    }

}
