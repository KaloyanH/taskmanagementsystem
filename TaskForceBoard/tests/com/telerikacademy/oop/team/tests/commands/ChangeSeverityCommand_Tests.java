package com.telerikacademy.oop.team.tests.commands;

import com.telerikacademy.oop.team.commands.contracts.Command;
import com.telerikacademy.oop.team.commands.creation.ChangeSeverityCommand;
import com.telerikacademy.oop.team.core.TaskRepositoryImpl;
import com.telerikacademy.oop.team.core.contracts.TaskRepository;
import com.telerikacademy.oop.team.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static com.telerikacademy.oop.team.commands.creation.ChangeSeverityCommand.EXPECTED_NUMBER_OF_ARGUMENTS;


/**
 * ChangeSeverityCommand arguments: {{taskID}} {{advance/revert}}
 */

public class ChangeSeverityCommand_Tests {

    private Command command;
    private TaskRepository repository;

    @BeforeEach
    public void before() {
        this.repository = new TaskRepositoryImpl();
        this.command = new ChangeSeverityCommand(repository);
    }

    @ParameterizedTest(name = "with arguments count: {0}")
    @ValueSource(ints = {EXPECTED_NUMBER_OF_ARGUMENTS - 1, EXPECTED_NUMBER_OF_ARGUMENTS + 1})
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected(int argumentsCount) {
        // Arrange
        List<String> arguments = TestUtilities.initializeListWithSize(argumentsCount);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }


}
