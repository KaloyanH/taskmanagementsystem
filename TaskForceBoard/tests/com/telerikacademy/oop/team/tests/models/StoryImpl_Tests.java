package com.telerikacademy.oop.team.tests.models;

import com.telerikacademy.oop.team.models.tasks.BugImpl;
import com.telerikacademy.oop.team.models.tasks.StoryImpl;
import com.telerikacademy.oop.team.models.tasks.contracts.Bug;
import com.telerikacademy.oop.team.models.tasks.contracts.Story;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;
import static com.telerikacademy.oop.team.tests.utils.TestUtilities.initializeStringWithSize;

public class StoryImpl_Tests {

    @Test
    public void StoryImpl_ShouldImplementStoryInterface() {
       Story story = new StoryImpl(0,"Story title", "Story description");
        // Assert
        Assertions.assertTrue(story instanceof Story);
    }

    @Test
    public void StoryImpl_ShouldImplementTaskInterface() {
        Story story = new StoryImpl(0,"Story title", "Story description");
        // Assert
        Assertions.assertTrue(story instanceof Task);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {TITLE_LENGTH_MIN_SYMBOLS - 1, TITLE_LENGTH_MAX_SYMBOLS + 1})
    public void should_throwException_when_StoryTitleLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(0, initializeStringWithSize(testLength), "Story description"));
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {DESCRIPTION_LENGTH_MIN_SYMBOLS - 1, DESCRIPTION_LENGTH_MAX_SYMBOLS + 1})
    public void should_throwException_when_StrotyDescriptionLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(0, "Story title", initializeStringWithSize(testLength)));
    }

}
