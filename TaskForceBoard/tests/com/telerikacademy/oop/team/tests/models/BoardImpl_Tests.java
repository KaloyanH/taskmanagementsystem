package com.telerikacademy.oop.team.tests.models;

import com.telerikacademy.oop.team.models.tasks.BoardImpl;
import com.telerikacademy.oop.team.models.tasks.contracts.Board;
import org.junit.Test;
//import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.NAME_MAX_LENGTH;
import static com.telerikacademy.oop.team.models.tasks.ModelConstants.NAME_MIN_LENGTH;
import static com.telerikacademy.oop.team.tests.utils.TestUtilities.initializeStringWithSize;

public class BoardImpl_Tests {

    @Test
    public void BoardImpl_ShouldImplementBoardInterface() {
        BoardImpl board = new BoardImpl("MyTeamBoard");
        // Assert
        Assertions.assertTrue(board instanceof Board);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {NAME_MIN_LENGTH - 1, NAME_MAX_LENGTH + 1})
    public void should_throwException_when_boardNameLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BoardImpl(initializeStringWithSize(testLength)));
    }

}
