package com.telerikacademy.oop.team.tests.models;

import com.telerikacademy.oop.team.models.tasks.HistoryImpl;
import com.telerikacademy.oop.team.models.tasks.contracts.History;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HistoryImpl_Tests {

    @Test
    public void HistoryImplImpl_ShouldImplementHistoryInterface() {
        History history = new HistoryImpl("Some event");
        // Assert
        Assertions.assertTrue(history instanceof History);
    }

    @Test
    public void should_throwException_when_HistoryDescriptionNull() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new HistoryImpl(null));
    }

}
