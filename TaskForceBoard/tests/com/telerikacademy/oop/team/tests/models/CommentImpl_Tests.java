package com.telerikacademy.oop.team.tests.models;

import com.telerikacademy.oop.team.models.tasks.CommentImpl;
import com.telerikacademy.oop.team.tests.utils.TestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import javax.xml.stream.events.Comment;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.CONTENT_LEN_MAX;
import static com.telerikacademy.oop.team.models.tasks.ModelConstants.CONTENT_LEN_MIN;
import static com.telerikacademy.oop.team.tests.utils.TestUtilities.initializeStringWithSize;

public class CommentImpl_Tests {

    @Test
    public void CommentImpl_ShouldImplementCommentInterface() {
        CommentImpl comment = new CommentImpl("content", "author");
        Assertions.assertTrue(comment instanceof Comment);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {CONTENT_LEN_MIN - 1, CONTENT_LEN_MAX + 1})
    public void should_throwException_when_modelNameLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new CommentImpl(
                        initializeStringWithSize(testLength),
                        "author"));
    }

}
