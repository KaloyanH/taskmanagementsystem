package com.telerikacademy.oop.team.tests.models;

import com.telerikacademy.oop.team.models.tasks.MemberImpl;
import com.telerikacademy.oop.team.models.tasks.contracts.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.NAME_MAX_LENGTH;
import static com.telerikacademy.oop.team.models.tasks.ModelConstants.NAME_MIN_LENGTH;
import static com.telerikacademy.oop.team.tests.utils.TestUtilities.initializeStringWithSize;

public class MemberImpl_Tests {

    @Test
    public void MemberImpl_ShouldImplementMemberInterface() {
        MemberImpl member = new MemberImpl("NewMemberName");
        // Assert
        Assertions.assertTrue(member instanceof Member);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {NAME_MIN_LENGTH - 1, NAME_MAX_LENGTH + 1})
    public void should_throwException_when_memberNameLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new MemberImpl(initializeStringWithSize(testLength)));
    }

}
