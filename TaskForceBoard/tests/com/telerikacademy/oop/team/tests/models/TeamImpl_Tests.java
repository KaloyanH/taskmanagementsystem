package com.telerikacademy.oop.team.tests.models;

import com.telerikacademy.oop.team.models.tasks.TeamImpl;
import com.telerikacademy.oop.team.models.tasks.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.NAME_MAX_LENGTH;
import static com.telerikacademy.oop.team.models.tasks.ModelConstants.NAME_MIN_LENGTH;
import static com.telerikacademy.oop.team.tests.utils.TestUtilities.initializeStringWithSize;

public class TeamImpl_Tests {

    @Test
    public void TeamImpl_ShouldImplementTeamInterface() {
        TeamImpl team = new TeamImpl("MyAwesomeTeam");
        // Assert
        Assertions.assertTrue(team instanceof Team);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {NAME_MIN_LENGTH - 1, NAME_MAX_LENGTH + 1})
    public void should_throwException_when_teamNameLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new TeamImpl(initializeStringWithSize(testLength)));
    }

}
