package com.telerikacademy.oop.team.tests.models;

import com.telerikacademy.oop.team.models.tasks.BugImpl;
import com.telerikacademy.oop.team.models.tasks.contracts.Bug;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;
import static com.telerikacademy.oop.team.tests.utils.TestUtilities.initializeStringWithSize;

public class BugImpl_Tests {

    @Test
    public void BugImpl_ShouldImplementBugInterface() {
        BugImpl bug = new BugImpl(0, "MyBugTitle", "MyBugDescription");
        // Assert
        Assertions.assertTrue(bug instanceof Bug);
    }

    @Test
    public void BugImpl_ShouldImplementTaskInterface() {
        BugImpl bug = new BugImpl(0, "MyBugTitle", "MyBugDescription");
        // Assert
        Assertions.assertTrue(bug instanceof Task);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {TITLE_LENGTH_MIN_SYMBOLS - 1, TITLE_LENGTH_MAX_SYMBOLS + 1})
    public void should_throwException_when_bugTitleLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(0, initializeStringWithSize(testLength), "MyBugDescription"));
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {DESCRIPTION_LENGTH_MIN_SYMBOLS - 1, DESCRIPTION_LENGTH_MAX_SYMBOLS + 1})
    public void should_throwException_when_bugDescriptionLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(0, "MyBugTitle", initializeStringWithSize(testLength)));
    }

}
