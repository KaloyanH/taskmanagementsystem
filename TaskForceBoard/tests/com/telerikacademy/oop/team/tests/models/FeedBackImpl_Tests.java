package com.telerikacademy.oop.team.tests.models;

import com.telerikacademy.oop.team.models.tasks.BugImpl;
import com.telerikacademy.oop.team.models.tasks.FeedBackImpl;
import com.telerikacademy.oop.team.models.tasks.contracts.FeedBack;
import com.telerikacademy.oop.team.models.tasks.contracts.Task;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.telerikacademy.oop.team.models.tasks.ModelConstants.*;
import static com.telerikacademy.oop.team.tests.utils.TestUtilities.initializeStringWithSize;

public class FeedBackImpl_Tests {

    @Test
    public void FeedBackImpl_ShouldImplementFeedBackInterface() {
        FeedBack feedBack = new FeedBackImpl(0, "Feedback title", "Feedback description", 5);
        // Assert
        Assertions.assertTrue(feedBack instanceof FeedBack);
    }

    @Test
    public void FeedBackImpl_ShouldImplementTaskInterface() {
        FeedBack feedBack = new FeedBackImpl(0, "Feedback title", "Feedback description", 5);
        // Assert
        Assertions.assertTrue(feedBack instanceof Task);
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {TITLE_LENGTH_MIN_SYMBOLS - 1, TITLE_LENGTH_MAX_SYMBOLS + 1})
    public void should_throwException_when_FeedBackTitleLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(0, initializeStringWithSize(testLength), "Feedback description"));
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {DESCRIPTION_LENGTH_MIN_SYMBOLS - 1, DESCRIPTION_LENGTH_MAX_SYMBOLS + 1})
    public void should_throwException_when_FeedBackDescriptionLengthOutOfBounds(int testLength) {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(0, "Feedback title", initializeStringWithSize(testLength)));
    }

}
