# Task Force - Task Management System

## Description

**Task Force** is a task management system console application. **Task Force** can be used by small teams of developers to keep track of tasks related to software development projects.

## Team Components

- **Team**: a team is a group of members working together. Each team can have one or more boards that contain the team's tasks. Task Force supports multiple teams, however, the team name must be unique within the application.

- **Member**: a member is a user that is a part of a team. Members can be assigned to more than one team. Member names must be unique within the application.
- **Board**: a board is a collection of tasks that are associated with a team.

## Task  Types

- **Bug**: contains information about an incorrect or undesired result that deviates from the expected result or behavior.

- **Story**: contains short requirements or requests written from the perspective of an end user.

- **Feedback**: contains information from customers on their likes, dislikes, impressions, and requests about a product.


## Usage
Task Force supports the following command functionality.

- AddCommentToTask *comment* *taskID*

  ```
  createbug {{This is the bug title}} {{This is the bug description}}
  createteam MyGreatTeam
  createmember Gosho
  addmembertoteam Gosho MyGreatTeam
  assigntask 1 Gosho
  addcommenttotask {{This bug is really annoying! Needs to be fixed.}} Gosho 1
  ```

- АddSteps *bugID* *StepToFixTheBug*

  ```
  createbug {{TheMOSTAnnoyingBug}} {{FixItByDebuging}}
  addsteps 1 {{Fix The BUG}}
  ```

- AddMemberToTeam *memberName* *teamName*

  ```
  createmember Gosho
  createteam MyGreatTeam
  addmembertoteam Gosho MyGreatTeam
  ```

- AddTaskToBoard *taskID* *teamName*

  ```
  createteam MyGreatTeam
  createboard MyTeamBoard MyGreatTeam
  createbug {{This is the bug title}} {{This is the bug description}}
  addtasktoboard 1 MyTeamBoard
  ```

- AssignTask *taskID* *AssigneeName*

  ```
  createmember Gosho
  createteam MyGreatTeam
  addmembertoteam Gosho MyGreatTeam
  createboard MyTeamBoard MyGreatTeam
  createbug {{This is the bug title}} {{This is the bug description}}
  addtasktoboard 1 MyTeamBoard
  assigntask 1 Gosho
  ```

- ChangePriority *bugID/storyID* advance/revert

  ```
  createstory {{Story title}} {{Story description}}
  createbug {{This is the bug title}} {{Bug description}}
  changepriority 1 advance
  changepriority 2 advance
  changepriority 1 revert
  changepriority 2 revert
  ```

- ChangeRating *feedbackID* *rating*

  ```
  createfeedback {{This is the feedback title}} {{This is the feedback description}} 5
  changerating 1 10
  ```

- ChangeSeverity *bugID* advance/revert

  ```
  createbug {{This is the bug title}} {{This is the bug description}}
  changeseverity 1 advance
  changeseverity 1 revert
  ```

- ChangeStatus *storyID* advance/revert

  ```
  createstory {{This is the story title}} {{This is the story description}}
  changesize 1 advance
  changesize 1 revert
  ```

- ChangeStatus *bugID/storyID* advance/revert

  ```
  createstory {{Story title}} {{Story description}}
  createbug {{This is the bug title}} {{Bug description}}
  changestatus 1 advance
  changestatus 2 advance
  changestatus 1 revert
  changestatus 2 revert
  ```

- CreateBoard *boardName* *teamName*

  ```
  createteam MyGreatTeam
  createboard MyTeamBoard MyGreatTeam
  ```

- CreateBug *tittle description assignee*

  ```
  createbug {{This is the bug title}} {{This is the bug description}}
  showtasksactivity 1
  ```

- CreateFeedback *title description rating*

  ```
  createfeedback {{This is the feedback title}} {{This is the feedback description}} 5
  showtasksactivity 1
  ```

- CreateMember *name*

  ```
  createmember Gosho
  ```

- CreateStory *title description assignee*

  ```
  createstory {{This is the story title}} {{This is the story description}}
  showtasksactivity 1
  ```

- CreateTeam *teamName*

  ```
  createteam MyGreatTeam
  ```

- ShowAllBoards *teamName*

  ```
  createteam MyGreatTeam
  createboard MyTeamBoard MyGreatTeam
  createboard MyNewTeamBoard MyGreatTeam
  showallboards MyGreatTeam
  ```

- ShowAllteamMembers *teamName*

  ```
  createteam MyGreatTeam
  createmember Gosho
  addmembertoteam Gosho MyGreatTeam
  showallteammembers MyGreatTeam
  ```

- ShowAllTeams

  ```
  createteam MyGreatTeam
  createteam AnotherTeam
  showallteams
  ```

- Showtasksactivity *taskId*

  ```
  createstory {{Story title}} {{Story description}}
  createbug {{This is the bug title}} {{Bug description}}
  createfeedback {{This is the Feedback title}} {{Feedback description}} 1
  showtasksactivity 1 
  showtasksactivity 2
  showtasksactivity 3
  ```

- ShowAllUsers

  ```
  createmember Gosho
  createmember Pesho
  showallusers
  ```

- ShowTeamsActivity *teamName*

  ```
  createteam MyGreatTeam
  showteamsactivity MyGreatTeam
  ```

- ShowUsersActivity *userName*

  ```
  createmember Gosho
  showusersactivity Gosho
  ```
  - ShowAllSteps *bugID*

  ```
  createbug {{The MOST Annoying Bug}} {{Fix It By Debuging}}
  addsteps 1 {{Fix The BUG}}
  addsteps 1 {{Mark It Fixed}}
  addsteps 1 {{UnAssign It}}
  showallsteps 1
  ```

- UnassignTask *taskID* **memberName**

  ```
  createmember Gosho
  createteam MyGreatTeam
  addmembertoteam Gosho MyGreatTeam
  createboard MyTeamBoard MyGreatTeam
  createbug {{This is the bug title}} {{This is the bug description}}
  addtasktoboard 1 MyTeamBoard
  assigntask 1 Gosho
  unassigntask 1 Gosho
  ```



## Authors
[Yordan Georgiev](georgiev.j@gmail.com) and [Kaloyan Haralampiev](kaloianharalampiev@gmail.com)



